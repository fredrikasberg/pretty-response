<?php

namespace Fasberg\PrettyResponse\Facade;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;
use Closure;
use Fasberg\PrettyResponse\PrettyResponse;

/**
 * Class PrettyResponseFacade
 *
 * @package Fasberg\PrettyResponse
 * @mixin PrettyResponse
 * @method conditional(Closure|Collection|bool|null $param, JsonResource|Resource|array|null|AnonymousResourceCollection $data, array|null $array)
 * @method wrap(JsonResource|Resource|array|null|AnonymousResourceCollection $data)
 */
class PrettyResponseFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fasberg.prettyresponse';
    }
}
