<?php

namespace Fasberg\PrettyResponse;

use Illuminate\Support\ServiceProvider;

/**
 * Class PrettyResponseServiceProvider
 *
 * @package Aring\PrettyResponse
 */
class PrettyResponseServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('fasberg.prettyresponse', function () {
            return new PrettyResponse();
        });

        $this->mergeConfigFrom(
            __DIR__.'/prettyresponse.config.php', 'prettyresponse'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes(
            [
                __DIR__.'/prettyresponse.config.php' => config_path('prettyresponse.php'),
            ],
            'config'
        );
    }


}
