<?php

namespace Fasberg\PrettyResponse;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Closure;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class PrettyResponse
{
    /**
     * Base response
     * @var array
     */
    private $response = [
        'content' => [
            'message' => null,
            'errors' => [],
        ],
        'code' => 200,
    ];

    /**
     * Options
     * @var array
     */
    private $options;

    /**
     * Check if input is a Closure
     *
     * @param $t mixed Input to check
     * @return bool Result
     */
    private function isClosure($t): bool
    {
        return $t instanceof Closure;
    }

    /**
     * Check if input is a Collection
     *
     * @param $t mixed Input to check
     * @return bool Result
     */
    private function isCollection($t): bool
    {
        return $t instanceof Collection;
    }

    /**
     * Checks if input is derived from ApiResource
     * @param $t mixed Input
     * @return bool Result
     */
    private function isApiResource($t): bool
    {
        return $t instanceof Resource;
    }

    /**
     * Checks if input is derived from JsonResource
     * @param $t mixed Input
     * @return bool Result
     */
    private function isJsonResource($t): bool
    {
        return $t instanceof JsonResource;
    }

    /**
     * Checks if input is resource collection
     * @param $t mixed Input
     * @return bool Result
     */
    private function isResourceCollection($t): bool
    {
        return $t instanceof AnonymousResourceCollection;
    }

    /**
     * Checks if input is an array
     * @param $t mixed Input
     * @return bool Result
     */
    private function isArray($t): bool
    {
        return is_array($t);
    }
    
    /**
     * Flatten the data array (removing any extra 'data' encapsulation)
     * @param $data array Input
     */
    private function flattenData(array $data) : array {
        if(array_key_exists('data', $data) && count($data) == 1) {
            $data = $data['data'];
        }
        return $data;
    }

    /**
     * Appends data to response based on type
     * @param $t array|Collection|Resource|mixed Input
     */
    private function appendData($t): void
    {
        // Check if we have any errors and if we do check if we should still append data
        if (!isset($this->options['always_append_data']) && !empty($this->response['content']['errors'])) {
            return;
        }

        if (self::isArray($t)) {
            $this->response['content']['data'] = self::flattenData($t);
        }

        if (self::isCollection($t)) {
            $this->response['content']['data'] = self::flattenData($t->toArray());
        }

        if (self::isApiResource($t)) {
            $this->response['content']['data'] = $t;
        }

        if (self::isJsonResource($t)) {
            $this->response['content']['data'] = $t;
        }

        if (self::isResourceCollection($t)) {
            $this->response['content']['data'] = $t;
        }

    }

    /**
     * Appends message to response
     * If any errors are present no message will be appended
     */
    private function appendMessage(): void
    {
        if (!empty($this->response['content']['errors'])) {
            return;
        }

        if (isset($this->options['success']['message'])) {
            $this->response['content']['message'] = $this->options['success']['message'];
        } else {
            $this->response['content']['message'] = config('prettyresponse.default_success_message');
        }

        if (isset($this->options['success']['code'])) {
            $this->response['code'] = $this->options['success']['code'];
        } else {
            $this->response['code'] = config('prettyresponse.default_success');
        }

    }

    /**
     * Appends errors to response
     * If no error is defined then the default error description will be used
     */
    private function appendError(): void
    {
        if (isset($this->options['error']['message'])) {
            $this->response['content']['errors'][] = $this->options['error']['message'];
        } else {
            $this->response['content']['errors'][] = config('prettyresponse.default_failure_message');
        }

        if (isset($this->options['error']['code'])) {
            $this->response['code'] = $this->options['error']['code'];
        } else {
            $this->response['code'] = config('prettyresponse.default_failure');
        }

    }

    /**
     * Build response using a closure
     *
     * @param  Closure  $status
     * @return void
     */
    protected function buildWithClosure(Closure $status): void
    {
        if (!$status()) {
            self::appendError();
        } else {
            self::appendMessage();
        }
    }

    /**
     * @param  Collection  $collection
     * @return void
     */
    protected function buildWithCollection(Collection $collection): void
    {
        if ($collection->isEmpty()) {
            self::appendError();
        } else {
            self::appendMessage();
        }
    }

    /**
     * @param $status mixed Input to check
     * @return void
     */
    private function buildDefault($status): void
    {
        if (!filter_var($status, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
            self::appendError();
        } else {
            self::appendMessage();
        }
    }

    /**
     * Build a new response using a variety of input for determining the success or failure of
     * the action
     *
     * @param $status
     * @param  null  $data
     * @param  array  $options
     * @return JsonResponse
     */
    public function conditional($status, $data = null, array $options = []): JsonResponse
    {
        $this->options = $options;

        if (self::isClosure($status)) {
            self::buildWithClosure($status);
        } elseif (self::isCollection($status)) {
            self::buildWithCollection($status);
        } else {
            self::buildDefault($status);
        }

        // The response should now be populated by an error or a success
        // We can now append data
        self::appendData($data);

        // And return response
        return response()->json($this->response['content'], $this->response['code']);
    }

    /**
     * Build a new response without any error checking and such
     *
     * @param  null  $data
     * @return JsonResponse
     */
    public function wrap($data = null)
    {
        self::appendData($data);
        self::appendMessage();

        return response()->json($this->response['content'], $this->response['code']);
    }
}
