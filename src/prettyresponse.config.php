<?php

return [

    // Default success
    'default_success' => 200,
    'default_success_message' => 'OK',

    // Default error
    'default_failure' => 422,
    'default_failure_message' => 'An error occurred while processing the request.'
];
