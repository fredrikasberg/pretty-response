## PrettyResponse

### Installation

```bash
composer require fasberg/prettyresponse
```

### Usage

```php
use Fasberg\PrettyResponse\Facade\PrettyResponseFacade as PrettyResponse;

...

return PrettyResponse::wrap($resource, $options);

return PrettyResponse::conditional($condition, $resource, $options);

```

#### Wrap

Will take an array, collection or resource and wrap it in the base success response

#### Conditional

Conditional variable can be boolean, Closure or Collection.

It will default to trying to convert input into a boolean.

Data should be in the form of an array, collection or resource.

Options allows you to set a custom error/success message and return code

```php
$options = [
    'error' => [
        'message' => 'My error message',
        'code' => 123
    ],
    'success' => [
        'message' => 'My success message',
        'code' => 321
    ]
];
```

#### Configuration

There is a configuration file available that will allow you to set global defaults.

Publish it by running

```bash
php artisan vendor:publish --provider="Fasberg\PrettyResponse\PrettyResponseServiceProvider"
```